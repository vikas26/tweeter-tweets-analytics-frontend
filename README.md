# This is a 24 hour test given for a firm

## Backend Code (Cakephp, MySQL) - [link](https://bitbucket.org/vikas26/tweeter-tweets-analytics-backend)
## Frontend Coxe (AngularJS, Materialize UI) - [link](https://bitbucket.org/vikas26/tweeter-tweets-analytics-frontend)

### A demo web app using AngularJS and materialize UI that does following things.

- It pulls Twitter data for "analytics" keyword as far as back in time Twitter api allows. It is the stored in mysql database. Your preference, mongo support default json storage.

- It is the time to build web app frontend. It shows following metrics on dashboard
	1. Daily and weekly number of tweets in line graph with a dropdown selector.
	2. Split of tweets by location in pie chart (percentages)

- Second page allows to search tweets by text (full text search). List of tweets are displayed on web page in tabular format sorted by tweet date. Additionally there is a date range filter if one needs to select tweets within date range.
