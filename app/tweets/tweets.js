'use strict';

angular.module('myApp.tweets', [
    'ngRoute',
    'chart.js',
    'myApp.fetchTweetsFactory'
])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/tweets', {
        templateUrl: 'tweets/tweets.html',
        controller: 'TweetsCtrl'
    });
}])

.controller('TweetsCtrl', ['$scope', '$filter', 'fetchTweetsFactory', function($scope, $filter, fetchTweetsFactory) {

    // my tweets chart data format
    let myTweetsChartData = {
        labels: [],
        series: ['Tweets count'],
        data: [
            []
        ],
        options: {}
    };

    // tweets by location
    let myTweetsLocationChartData = {
        labels: [],
        data: [],
        pieceLabel: {
            mode: 'percentage',
            precision: 2
        }
    }

    // prepare tweets chart
    let prepareTweetsChart = function () {
        // reset labels and points
        myTweetsChartData.labels.length     = 0;
        myTweetsChartData.data[0].length    = 0;

        // set labels and points
        if ($scope.selectedState == "daily") {
            // daily
            $scope.tweets.map(function(tweet) {
                let date = $filter('date')(tweet[0].tweeted_on, 'dd-MM-yyyy');
                myTweetsChartData.labels.unshift(date);
                myTweetsChartData.data[0].unshift(tweet[0].tweets_count);
            });
        } else {
            // weekly
            $scope.tweets.map(function(tweet) {
                let weekSD = new Date(tweet[0].date);
                let weekLD = angular.copy(weekSD);
                weekLD.setDate(weekLD.getDate() + 6);

                let week = $filter('date')(weekSD, 'dd-MM-yy') + 
                            ' - ' + 
                            $filter('date')(weekLD, 'dd-MM-yy');

                myTweetsChartData.labels.unshift(week);
                myTweetsChartData.data[0].unshift(tweet[0].tweets_count);
            });
        }

        // set chart data
        $scope.myTweetsChart = myTweetsChartData;
    };

    // prepare location chart
    let prepareLocationChart = function () {
        // reset labels and points
        myTweetsLocationChartData.labels.length = 0;
        myTweetsLocationChartData.data.length   = 0;

        let totalTweets = 0;
        $scope.tweets.map(function(tweet) {
            totalTweets += parseInt(tweet[0].tweets_count);
            myTweetsLocationChartData.labels.unshift(tweet.Tweet.location);
            myTweetsLocationChartData.data.unshift(tweet[0].tweets_count);
        });

        // in percentage
        myTweetsLocationChartData.data = myTweetsLocationChartData.data.map(function(x) { return parseFloat(parseInt(x) * 100 / totalTweets).toFixed(2); });

        // set chart data
        $scope.myTweetsLocationChart = myTweetsLocationChartData;
    };

    // getch daily tweets
    let fetchDailyTweets = function () {
        return fetchTweetsFactory.fetchDaily().then(function(response) {
            let tweetsData      = angular.fromJson(response.data);
            $scope.page         = tweetsData.page;
            $scope.tweets       = tweetsData.dailyTweets;
        });
    };

    // getch weekly tweets
    let fetchWeeklyTweets = function () {
        return fetchTweetsFactory.fetchWeekly().then(function(response) {
            let tweetsData      = angular.fromJson(response.data);
            $scope.page         = tweetsData.page;
            $scope.tweets       = tweetsData.weeklyTweets;
        });
    };

    // getch weekly tweets
    let fetchTweetsByLocation = function () {
        return fetchTweetsFactory.fetchByLocation().then(function(response) {
            let tweetsData      = angular.fromJson(response.data);
            $scope.page         = tweetsData.page;
            $scope.tweets       = tweetsData.tweets;
        });
    };

    // view stats types
    $scope.statsStates = [
        { name: 'Daily',  value: 'daily'  },
        { name: 'Weekly', value: 'weekly' }
    ];

    // default state
    $scope.selectedState = 'daily';

    // on change of state
    $scope.stateChanged = function() {
        if ($scope.selectedState == "daily") {
            // load daily tweets
            fetchDailyTweets().then(function() {
                prepareTweetsChart ();
            });
        } else {
            // load weekly tweets
            fetchWeeklyTweets().then(function() {
                prepareTweetsChart ();
            });
        }
    };

    // load daily tweets
    fetchDailyTweets().then(function() {
        prepareTweetsChart ();
    });

    // is location data fetched
    $scope.isLocationDataAvailable = false;

    // on change of tab (count, location)
    $scope.analyticsType = function (type) {
        // location tab and data not loaded yet
        if ($scope.isLocationDataAvailable == false && type == 'location') {
            $scope.isLocationDataAvailable = true;
            fetchTweetsByLocation().then(function() {
                prepareLocationChart ();
            });
        }
    };
}]);