'use strict';

angular.module('myApp.searchTweets', [
    'ngRoute',
    'myApp.fetchSearchTweetsFactory'
])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/search-tweet', {
        templateUrl: 'search-tweets/view.html',
        controller: 'SearchTweetCtrl'
    });
}])

.controller('SearchTweetCtrl', ['$scope', '$timeout', '$filter', 'fetchSearchFactory', function($scope, $timeout, $filter, fetchSearchFactory) {

    $scope.searchText       = '';
    $scope.firstDate        = '';
    $scope.lastDate         = '';
    $scope.resultsFound     = false;
    $scope.zeroResultsFound = false;

    // last search promise
    let searchPromise = null;

    // on change of search text inout field
    $scope.searchTweet = function () {
        // cancel previous timeout
        $scope.resultsFound     = false;
        $scope.zeroResultsFound = false;

        if ($scope.searchText.length < 1) return;

        $timeout.cancel(searchPromise);
        searchPromise = $timeout(searchNow, 500); 
    };

    // search now
    let searchNow = function () {
        let data = {
            'q': $scope.searchText,
            'startDate': $filter('date')(new Date($scope.firstDate).getTime(), 'yyyy-MM-dd'),
        };

        // last date
        let lastDate = new Date($scope.lastDate);
        lastDate.setDate(lastDate.getDate() + 1);
        data['lastDate'] = $filter('date')(lastDate.getTime(), 'yyyy-MM-dd');

        // fetch searched results
        fetchSearchFactory.fetchTweet(data)
            .then(function(response) {
                let tweetsData      = angular.fromJson(response.data);
                $scope.tweets       = tweetsData.tweets;
                $scope.resultsFound = true;
                
                console.log($scope.tweets.length);
                if ($scope.tweets.length == 0) {
                    $scope.zeroResultsFound = true;
                }
            });
    };

    // format date
    $scope.strToDate = function (dateStr) {
        return $filter('date')(new Date(dateStr).getTime(), 'dd-MM-yyyy');
    };

}]);