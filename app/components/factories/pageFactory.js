angular.module('myApp.pageFactory', []).factory('pageFactory', function() {
    var title = 'Valiance | Vikas';
    return {
        title: function() { return title; },
        setTitle: function(newTitle) { title = newTitle; }
    };
});
