angular.module('myApp.fetchSearchTweetsFactory', [
    'app.myConstants',
])

.factory('fetchSearchFactory', ['$http', 'constants', function($http, constants) {
    return {
        fetchTweet: function(postData) {
            return $http({
                method: 'POST',
                url: constants.apiBaseUrl + '/tweets/search/',
                data: postData
            });
        },
    };
}]);
