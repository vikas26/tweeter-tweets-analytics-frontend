angular.module('myApp.fetchTweetsFactory', [
    'app.myConstants',
])

.factory('fetchTweetsFactory', ['$http', 'constants', function($http, constants) {
    return {
        fetchDaily: function() {
            return $http({
                method: 'GET',
                url: constants.apiBaseUrl + '/tweets/daily/'
            });
        },
        fetchWeekly: function() {
            return $http({
                method: 'GET',
                url: constants.apiBaseUrl + '/tweets/weekly/'
            });
        },
        fetchByLocation: function() {
            return $http({
                method: 'GET',
                url: constants.apiBaseUrl + '/tweets/location/'
            });
        }
    };
}]);
