'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'ngMaterial',
    'myApp.sideNav',
    'myApp.tweets',
    'myApp.searchTweets',
])

.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider.otherwise({
        redirectTo: '/tweets'
    });
}])

.run(['$rootScope', function($rootScope) {
    $rootScope.pageTitle = 'Vikas';
}]);
